/**
 * @description Contexto para express js
 */

import { Request, Response } from 'express'

export interface Ctx {
  req: Request
  res: Response
  payload?: { userId: string }
}
