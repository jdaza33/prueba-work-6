/**
 * @description Configuracion para seguridad
 */

// Modules
import bcrypt from 'bcryptjs'
import jwt from 'jwt-simple'
import moment from 'moment'

export { encryptPassword, comparePassword, encodeUser, decodeToken }

/**
 * @description Encripta una cadena de caracteres
 * @param {Clave sin encriptar} password
 */
const encryptPassword = function (password: string): string {
  let salt = bcrypt.genSaltSync(10)
  return bcrypt.hashSync(password, salt)
}

/**
 * @description Compara un string con un hash
 * @param {Clave sin encriptar} password
 * @param {Clave encriptada} hash
 */
function comparePassword(password: string, hash: string) {
  try {
    return bcrypt.compareSync(password, hash)
  } catch (error) {
    console.log(error)
  }
}

/**
 * @description Crea un token JWT con los datos del usuario
 * @param {Identificador del usuario} userId
 * @param {Rol del usuario} role
 */
function encodeUser(userId: number) {
  try {
    let payload = {
      id: userId,
      exp: moment().add(1, 'days').valueOf(), //Fecha de expiracion en milisegundos
    }
    return jwt.encode(payload, 'PRUEBA_DE_TRABAJO') //process.env.SECRET_JWT
  } catch (error) {
    console.log(error)
  }
}

/**
 * @description Decodifica un token JWT
 * @param {*} token
 */
function decodeToken(token: string) {
  try {
    return jwt.decode(token, 'PRUEBA_DE_TRABAJO') //process.env.SECRET_JWT
  } catch (error) {
    console.log(error)
  }
}
