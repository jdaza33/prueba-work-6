/**
 * @description Configuración de TypeORM
 */

//Modules
import { createConnection } from 'typeorm'
import path from 'path'

export async function connect() {
  await createConnection({
    type: 'mysql',
    host: 'sql5.freemysqlhosting.net',
    port: 3306,
    username: 'sql5420503',
    password: 'tL2IKnYju8',
    database: 'sql5420503',
    entities: [path.join(__dirname, '../schemas/**/**{.ts,.js}')],
    synchronize: true,
    // logging: true,
  })
  console.log('Database is connected')
}
