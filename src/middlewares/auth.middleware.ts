/**
 * @description Middleware de autenticación
 */

//Modules
import { MiddlewareFn } from 'type-graphql'

//Utils
import { decodeToken } from '../config/security.config'

//Context
import { Ctx } from '../config/ctx-express.config'

/**
 * FORMAT --> Bearer {token}
 */
export const isAuth: MiddlewareFn<Ctx> = ({ context }, next) => {
  try {
    const authorization: string = context.req.headers['authorization']!
    if (!authorization) throw new Error('Necesita un token')

    const token: string = authorization.split(' ')[1]
    if (!token) throw new Error('Necesita un token valido. Ej: Bearer 123456')

    const { id: userId, exp } = decodeToken(token)
    const now = Date.now()

    if (now > exp) throw new Error('Su token ya expiro')

    context.payload = { userId } as any

    return next()
  } catch (err) {
    console.log(err)
    throw new Error('No está autenticado')
  }
}
