/**
 * @description Prueba de trabajo
 * @author José Bolívar
 */

//Modules
import 'reflect-metadata'
import { connect } from './config/typeorm.config'
import { startServer } from './app'

async function main() {
  const app = await startServer()
  await connect()
  app.listen(3001)
  console.log('Server on PORT: ' + 3001)
}

main().catch((err) => console.log(err))
