/**
 * @description Precarga de los archivos .env
 */

//Modules
import * as dotenv from 'dotenv'
import path from 'path'

//Constants
const { parsed: env } = dotenv.config()

const startDotenv = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const { parsed: ENV } = dotenv.config({
        path: path.resolve(__dirname, '../', `.env.${process.env.NODE_ENV}`),
      })

      process.env = { ...process.env, ...ENV }
      return resolve({ ...ENV })
    } catch (error) {
      return reject(error)
    }
  })
}

export { startDotenv }
