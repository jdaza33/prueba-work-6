/**
 * @description Controlador de usuarios
 */

//Modules
import { Resolver, Query, Mutation, Arg, Field, InputType } from 'type-graphql'

//Utils
import {
  encryptPassword,
  comparePassword,
  encodeUser,
} from '../config/security.config'

//Schemas
import { User } from '../schemas/User'

@InputType()
class UserInput {
  @Field()
  name!: string

  @Field()
  email!: string

  @Field()
  password!: string
}

@Resolver()
export class UserResolver {
  @Mutation(() => User)
  async signUp(@Arg('userIn', () => UserInput) userIn: UserInput) {
    try {
      userIn.password = encryptPassword(userIn.password)
      const user = User.create(userIn)
      return await user.save()
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => String)
  async login(@Arg('email') email: string, @Arg('password') password: string) {
    try {
      const user = await User.findOne({ email })
      if (!comparePassword(password, user?.password!))
        return 'Contraseña incorrecta'
      return encodeUser(user?.id!)
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
