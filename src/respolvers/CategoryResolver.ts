/**
 * @description Controlador de categorias
 */

//Modules
import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Field,
  InputType,
  UseMiddleware,
} from 'type-graphql'

//Schemas
import { Category } from '../schemas/Category'

//Middlewares
import { isAuth } from '../middlewares/auth.middleware'

@InputType()
class CategoryInput {
  @Field()
  name!: string
}

@Resolver()
export class CategoryResolver {
  @Query(() => [Category])
  @UseMiddleware(isAuth)
  async getCategories() {
    try {
      return await Category.find()
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => Category)
  @UseMiddleware(isAuth)
  async createCategory(
    @Arg('categoryIn', () => CategoryInput) categoryIn: CategoryInput
  ) {
    try {
      const category = Category.create(categoryIn)
      return await category.save()
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Query(() => Category)
  @UseMiddleware(isAuth)
  async getOneCategory(@Arg('id') id: number) {
    try {
      const category = await Category.findOne({ id })
      return category
        ? category
        : {
            id,
            name: '',
          }
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => Category)
  @UseMiddleware(isAuth)
  async updateCategory(
    @Arg('id') id: number,
    @Arg('data') data: CategoryInput
  ) {
    try {
      const category = await Category.findOne({ id })
      if (!category) throw new Error('ID de la categoria invalido')

      Object.assign(category, data)
      await category.save()

      return category
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async deleteCategory(@Arg('id') id: number) {
    try {
      const category = await Category.findOne({ id })
      if (!category) throw new Error('ID de la categoria invalido')
      await category.remove()

      return true
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
