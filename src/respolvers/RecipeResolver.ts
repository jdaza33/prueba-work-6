/**
 * @description Controlador de recetas
 */

//Modules
import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Field,
  InputType,
  UseMiddleware,
  Ctx,
} from 'type-graphql'

//Schemas
import { Recipe } from '../schemas/Recipe'

//Middlewares
import { isAuth } from '../middlewares/auth.middleware'

//Context
import { Ctx as Context } from '../config/ctx-express.config'

@InputType()
class RecipeInput {
  @Field()
  name!: string

  @Field()
  description!: string

  @Field()
  ingredients!: string

  @Field()
  category!: number
}

@InputType()
class RecipeUpdateInput {
  @Field({ nullable: true })
  name?: string

  @Field({ nullable: true })
  description?: string

  @Field({ nullable: true })
  ingredients?: string

  @Field({ nullable: true })
  category?: number
}

@Resolver()
export class RecipeResolver {
  @Query(() => [Recipe])
  @UseMiddleware(isAuth)
  async getRecipes() {
    try {
      return await Recipe.find()
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => Recipe)
  @UseMiddleware(isAuth)
  async createRecipe(
    @Arg('recipeIn', () => RecipeInput) recipeIn: RecipeInput,
    @Ctx() { payload }: Context
  ) {
    try {
      const recipe = Recipe.create({
        ...recipeIn,
        ...{ createdBy: parseInt(payload?.userId!) },
      })
      return await recipe.save()
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Query(() => Recipe)
  @UseMiddleware(isAuth)
  async getOneRecipe(@Arg('id') id: number) {
    try {
      const recipe = await Recipe.findOne({ id })
      return recipe
        ? recipe
        : {
            id,
            name: '',
            description: '',
            ingredients: '',
            category: 0,
            createdBy: 0,
          }
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Query(() => [Recipe])
  @UseMiddleware(isAuth)
  async getMyRecipes(@Ctx() { payload }: Context) {
    try {
      return await Recipe.find({ createdBy: parseInt(payload?.userId!) })
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => Recipe)
  @UseMiddleware(isAuth)
  async updateRecipe(
    @Arg('id') id: number,
    @Arg('data') data: RecipeUpdateInput
  ) {
    try {
      const recipe = await Recipe.findOne({ id })
      if (!recipe) throw new Error('ID de la receta invalido')

      Object.assign(recipe, data)
      await recipe.save()

      return recipe
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async deleteRecipe(@Arg('id') id: number) {
    try {
      const recipe = await Recipe.findOne({ id })
      if (!recipe) throw new Error('ID de la receta invalido')
      await recipe.remove()

      return true
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
