/**
 * @description Servidor
 */

//Modules
import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { buildSchema } from 'type-graphql'

//Resolvers
import { UserResolver } from './respolvers/UserResolver'
import { RecipeResolver } from './respolvers/RecipeResolver'
import { CategoryResolver } from './respolvers/CategoryResolver'

export async function startServer() {
  try {
    const app = express()

    const server = new ApolloServer({
      schema: await buildSchema({
        resolvers: [UserResolver, RecipeResolver, CategoryResolver],
        validate: false,
      }),
      context: ({ req, res }) => ({ req, res }),
    })

    server.applyMiddleware({ app, path: '/graphql' })

    return app
  } catch (error) {
    console.log(error)
    throw error
  }
}
