/**
 * @description Esquemas de recetas
 */

//Modules
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToOne,
  ManyToOne,
} from 'typeorm'
import { ObjectType, Field, ID } from 'type-graphql'

//Schemas
import { Category } from './Category'
import { User } from './User'

@Entity()
@ObjectType()
export class Recipe extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id!: number

  @Field(() => String)
  @Column()
  name!: string

  @Field(() => String)
  @Column()
  description!: string

  @Field(() => String)
  @Column()
  ingredients!: string

  @OneToOne(() => Category)
  @Field(() => Number)
  @Column()
  category!: number

  @ManyToOne(() => User)
  @Field(() => Number)
  @Column()
  createdBy!: number
}
