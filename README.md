# Prueba de Trabajo - The Puzzle

> Un servidor en GraphQL, TypeScript, TypeORM y NodeJS

## Setup

```sh
# Install dependencies
npm install

# Build typescript
npm run build

# Run dev
npm run dev

# Run prod
npm start
```